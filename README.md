# Gitpod Workspace Utilities

| Status | Maintainer/s |
| --- | -- |
| Experimential, due to Bashbox is an  | Andrei Jiroh Halili (@ajhalili2006) |

This repository contains the utilities we built for Gitpod workspaces for users and its underlying images for image maintainers, complied using Bashbox
to turn our modular code into an single file suitable for distribution online.

Versions for each utility is being tracked and tagged separately, but can be easily installed through the `gp-utils` script.

## Packages

* [`gp-utils`](./packages/gp-utils) - Our one-script-to-rule-them-all utility to install other packages in the repo to your system/container.
* [`gp-devenv`](./packages/gp-devenv) - Build workspace images and even test them before commiting your `.gitpod.yml` changes.

## Install

* Gitpod workspace utilities come preloaded in Gitpodified Workspace Images maintained under the Gitpodify project, make sure to use images
from `gitpodified-workspace-images` namespace in RHQCR.
  * If building an image with one of our images as base, either use `base` or `full` images to keep updated. Alternatively, run `gp-utils update` to update
the scripts to your heart's content.
* Other Docker/OCI images: Add the following lines in your Dockerfile

  ```dockerfile
  # if you forked this, set the build arg for gpWsUtils to point to your fork + /raw/main/dist/stable/gp-utils
  # you can also opt to go to weeky nigthly builds if needed.
  ARG gpWsUtils=https://gitlab.com/gitpodify/workspace-utilities/raw/main/dist/stable/gp-utils
  # --prefix=/usr/local/bin - Installs gp-utils-installer to /usr/local/bin, which requires root perms in the RUN step below.
  #                           By default, scripts will be installed to ~/.local/bin
  RUN curl --proto '=https' --tlsv1.2 -sSfL ${gpWsUtils} | bash install --prefix=/usr/local/bin
  ```

  Alternatively, install the scripts one by one using wget if preferred not to use `gp-utils-installer`.

  ```dockerfile
  ARG gpWsUtilsBaseUrl=https://gitlab.com/gitpodify/workspace-utilities/raw/main/dist/stable
  RUN wget -O /home/gitpod/.local/bin/gp-devenv ${gpWsUtilsBaseUrl}/gp-devenv \
      # Don't forget to make it executable
      && chmod +x /home/gitpod/.local/bin/gp-devenv
  ```

## Development

### Prerequisites for building from source

* bashbox (can be installed from the bashbox source by `git submodule update --init && ./utils/bashbox/bashbox -s selfinstall`)
* Bash installed, or atleast ones that support Bash syntax.

### Instructions

1. Install Bash on your system then install Bashbox (see prerequisites section for above command). reload your current shell session
by sourcing `~/.<bash|zsh>rc` file.
2. Build with debug stuff using `./hack/bashbox-complie.sh debug` or in production with just `./hack/bashbox-complie.sh`.
  * Debug builds can be found at `./build/debug` directory while production builds at `./build/prod`. In an CI setting, we move them to `dist` directory
via scheduled builds on weekly basis.
